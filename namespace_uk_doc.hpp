//
// Created by Tomas Silveira Salles on 29/05/17.
//

#ifndef LIBUKODUS_NAMESPACE_UK_DOC_HPP
#define LIBUKODUS_NAMESPACE_UK_DOC_HPP

/**
 * @file namespace_uk_doc.hpp
 *
 * @author <a href="mailto:tomasssalles@gmail.com">Tomas Silveira Salles</a>
 *
 * This header is only meant to provide minimal documentation for the @c UK namespace,
 * so out-of-class functions in this namespace are properly documented.
 */

/**
 * @namespace UK
 *
 * The general namespace for the Ukodus project.
 */
namespace UK {}

#endif /* LIBUKODUS_NAMESPACE_UK_DOC_HPP */
