# Ukodus project

Ukodus is a reverse game of sudoku, where the player tries to set the board for a sudoku game as difficult as possible
and then the machine has the task of solving it as quickly as possible.

The goal of this project is to provide the libraries implementing the necessary abstractions and algorithms.

## The libraries:

### `utils`

Here you'll find some helpful classes and methods that have nothing to do with the game itself, and could just
as well be in an external library. For example, `UK::PrettyTime<...>` is a wrapper around `std::chrono::duration<...>` 
which helps print duration values in a nice, human readable format, with units and all.
