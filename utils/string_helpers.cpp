//
// Created by Tomas Silveira Salles on 26/05/17.
//

#include "string_helpers.hpp"

#include <algorithm>

namespace UK
{
	std::string &
	StringUtils::remove_characters (std::string & str, std::initializer_list<char> chars_to_remove)
	{
		std::string::iterator new_end = std::remove_if(
			str.begin(), str.end(), [&chars_to_remove] (char c)
			{
				return (std::find(chars_to_remove.begin(), chars_to_remove.end(), c) != chars_to_remove.end());
			});

		str.erase(new_end, str.end());
		return str;
	}
} // namespace UK
