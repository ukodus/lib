//
// Created by Tomas Silveira Salles on 26/05/17.
//

#ifndef LIBUKODUS_STRING_HELPERS_HPP
#define LIBUKODUS_STRING_HELPERS_HPP

/**
 * @file string_helpers.hpp
 *
 * @author <a href="mailto:tomasssalles@gmail.com">Tomas Silveira Salles</a>
 */

#include <string>
#include <initializer_list>

namespace UK
{

	/**
	 * @struct StringUtils
	 *
	 * A collection of convenience methods implementing common operations with strings.
	 */
	struct StringUtils final
	{
		/**
		 * @brief Remove every occurrence of every character in the list @c chars_to_remove from the
		 * string @c str.
		 *
		 * This method implements the common idiom of combining @c std::remove_if and @c std::string::erase,
		 * and makes the syntax very comfortable. Example
		 *
		 * @code
		 * 	std::string hello = "Hello, World!";
		 * 	std::cout << UK::StringUtils::remove_characters(hello, {'l', 'o'}) << std::endl;
		 * @endcode
		 *
		 * Output:
		 *
		 * @code He, Wrd! @endcode
		 *
		 * @param str A string from which we wish to remove certain characters
		 * @param chars_to_remove A list of characters that should be removed from @c str
		 * @return The string @c str after it has been modified
		 */
		static std::string &
		remove_characters (std::string & str, std::initializer_list<char> chars_to_remove);
	};

} // namespace UK

#endif /* LIBUKODUS_STRING_HELPERS_HPP */
