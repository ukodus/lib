//
// Created by Tomas Silveira Salles on 26/05/17.
//

#include "ostream_wrapper.hpp"

namespace UK
{

	OStream::OStream (std::ostream & inner_)
		: _inner(inner_) {}

	std::ostream &
	OStream::inner () const
	{
		return _inner;
	}

	OStream &
	coutw ()
	{
		static OStream ret(std::cout);
		return ret;
	}

	OStream &
	cerrw ()
	{
		static OStream ret(std::cerr);
		return ret;
	}

} // namespace UK
