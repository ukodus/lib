//
// Created by Tomas Silveira Salles on 28/05/17.
//

#ifndef LIBUKODUS_RETCODE_HPP
#define LIBUKODUS_RETCODE_HPP

/**
 * @file retcode.hpp
 *
 * @author <a href="mailto:tomasssalles@gmail.com">Tomas Silveira Salles</a>
 *
 * Return codes with meaningful names and human-readable output (when output to @c UK::OStream).
 */

#include <utils/ostream_wrapper.hpp>

#include <stdexcept>

namespace UK
{

	/**
	 * @enum RetCode
	 *
	 * Constants with meaningful names for return codes, so we don't have to return boolean
	 * values to represent success or failure of a function.
	 */
	enum class RetCode
	{
		success,
		failure
	};

	/**
	 * This convenience method simplifies and clarifies the syntax when a method returns a
	 * @ref RetCode but internally decides whether it was successful through a boolean expression.
	 *
	 * @return @c RetCode::success if the condition is true, @c RetCode::failure otherwise.
	 */
	inline
	RetCode
	success_if (bool condition)
	{
		return condition ? RetCode::success : RetCode::failure;
	}

	/**
	 * Overload of @c output for @ref RetCode so that <code>out << rc;</code> prints a
	 * human-readable string when @c out is an @c OStream and @c rc is a @ref RetCode.
	 */
	inline
	void
	output (OStream & out, RetCode rc)
	{
		switch (rc)
		{
			case RetCode::success:
				out << "success"; break;
			case RetCode::failure:
				out << "failure"; break;
			default:
				throw std::invalid_argument("Unknown return code output to stream.");
		}
	}

} // namespace UK

#endif /* LIBUKODUS_RETCODE_HPP */
