//
// Created by Tomas Silveira Salles on 28/05/17.
//

#ifndef LIBUKODUS_GRID_PRINTER_HPP
#define LIBUKODUS_GRID_PRINTER_HPP

/**
 * @file grid_printer.hpp
 *
 * @author <a href="mailto:tomasssalles@gmail.com">Tomas Silveira Salles</a>
 */

#include <utils/ostream_wrapper.hpp>

#include <cassert>

namespace UK
{

	/**
	 * @class PrettyGrid
	 *
	 * @note It is much more convenient to use the factory method @ref as_pretty_grid than using this
	 * class's constructor directly, so you should check it out.
	 *
	 * This is a simple wrapper around a range of values (i.e. it holds iterators @c begin and @c end) for which
	 * we overload the output method into @c OStream in such a way that the values are printed as a nice looking
	 * grid (whose width is defined on construction of the @c PrettyGrid). For example, the fragment
	 *
	 * @code
	 * 	OStream & out = coutw();
	 * 	std::array<int, 5> const arr = {1, 22, 333, 44, 5};
	 * 	unsigned const num_elements_per_row = 3;
	 * 	unsigned const entry_width = 3;
	 * 	out << PrettyGrid<std::array<int, 5>::const_iterator>(
	 * 	   arr.begin(), arr.end(), num_elements_per_row, entry_width);
	 * @endcode
	 *
	 * will write something like the following to the @c OStream:
	 *
	 * @code
	 * 	+-----+-----+-----+
	 * 	|   1 |  22 | 333 |
	 * 	+-----+-----+-----+
	 * 	|  44 |   5 |     |
	 * 	+-----+-----+-----+
	 * @endcode
	 *
	 * The precise details of the output are not documented here, so small changes may happen in future versions.
	 * Nevertheless, we note the following points:
	 * 	- The lines between rows of the grid (in this example consisting of '+'s and '-'s) are called *separating
	 * 	lines*. The output always begins and ends with a separating line.
	 * 	- If the range has a number of entries that is not a multiple of the number of tiles per row, the last row
	 * 	will be completed with empty tiles.
	 * 	- If the given range is empty, only a separating line will be printed (not a row of empty tiles).
	 * 	- The output ends with a new line!
	 * 	- The output will not be aligned if it does not start on a new line!
	 *
	 * @note If you also wish to print your values with some additional format (say @c bool with @c boolalpha, or
	 * @c double with a certain @c precision) you can set this format on the stream before outputting the grid.
	 * The output method that prints the grid is guaranteed to only modify the stream's @c width parameter, and is
	 * also guaranteed to reset said @c width to its initial value before returning.
	 *
	 * @tparam InputIt An input iterator type for the range of values to be printed.
	 */
	template <typename InputIt>
	class PrettyGrid final
	{
	public:

		static unsigned const default_entry_width = 1;

		/**
		 * @note Don't use this directly. The method @ref as_pretty_grid is more convenient!
		 *
		 * @param begin_ Iterator at the beginning of the range of values to be printed.
		 * @param end_ Iterator at the past-the-end position of the range of values to be printed.
		 * @param max_elements_per_row_ The number of tiles in each row of the printed grid.
		 * @param entry_width_ The width used to print each value. (Must be large enough to accomodate each
		 * printed value or the grid will not be aligned!)
		 */
		PrettyGrid (
			InputIt begin_, InputIt end_, unsigned max_elements_per_row_, unsigned entry_width_ = default_entry_width);

		friend void
		output (OStream & out, PrettyGrid<InputIt> const & pg)
		{
			pg.write_to(out);
		}

	private:
		void
		write_to (OStream & out) const;

		InputIt
		begin () const;

		InputIt
		end () const;

		unsigned
		max_elements_per_row () const;

		unsigned
		entry_width () const;

		InputIt const _begin;
		InputIt const _end;
		unsigned const _max_elements_per_row;
		unsigned const _entry_width;
	};

	/**
	 * Use this method to easily construct a @c PrettyGrid without having to write the
	 * template parameter @c InputIt. Example:
	 *
	 * @code
	 * 	OStream & out = coutw();
	 * 	std::array<int, 5> const arr = {1, 22, 333, 44, 5};
	 * 	out << as_pretty_grid(arr.begin(), arr.end(), 3, 3);
	 * @endcode
	 *
	 * @note If you also wish to print your values with some additional format (say @c bool with @c boolalpha, or
	 * @c double with a certain @c precision) you can set this format on the stream before outputting the grid.
	 * The output method that prints the grid is guaranteed to only modify the stream's @c width parameter, and is
	 * also guaranteed to reset said @c width to its initial value before returning.
	 *
	 * @tparam InputIt An input iterator type for the range of values to be printed.
	 * @param begin Iterator at the beginning of the range of values to be printed.
	 * @param end Iterator at the past-the-end position of the range of values to be printed.
	 * @param max_elements_per_row The number of tiles in each row of the printed grid.
	 * @param entry_width The width used to print each value. (Must be large enough to accomodate each
	 * printed value or the grid will not be aligned!)
	 * @return An instance of @c PrettyGrid ready to be written to an @c OStream.
	 */
	template <typename InputIt>
	PrettyGrid<InputIt>
	as_pretty_grid (
		InputIt begin,
		InputIt end,
		unsigned max_elements_per_row,
		unsigned entry_width = PrettyGrid<InputIt>::default_entry_width);


	//BEGIN: Template implementation

	template <typename InputIt>
	PrettyGrid<InputIt>::PrettyGrid (InputIt begin_, InputIt end_, unsigned max_elements_per_row_, unsigned entry_width_)
		: _begin(begin_), _end(end_), _max_elements_per_row(max_elements_per_row_), _entry_width(entry_width_)
	{
		assert(entry_width() != 0);
	}

	template <typename InputIt>
	void
	PrettyGrid<InputIt>::write_to (OStream & out) const
	{
		std::string const separating_line_core(entry_width(), '-');
		std::string const empty_tile_core(entry_width(), ' ');

		auto print_separating_line = [this, &out, &separating_line_core] ()
		{
			for (unsigned column = 0; column < max_elements_per_row(); ++column)
			{
				out << "+-" << separating_line_core << '-';
			}
			out << '+' << std::endl;
		};

		auto print_single_row = [this, &out, &empty_tile_core] (InputIt & it)
		{
			unsigned column = 0;

			for (; (it != end()) and (column < max_elements_per_row()); ++it, ++column)
			{
				out << "| " << std::setw(entry_width()) << (*it) << std::setw(0) << ' ';
			}

			for (; column < max_elements_per_row(); ++column)
			{
				out << "| " << empty_tile_core << ' ';
			}

			out << '|' << std::endl;
		};

		auto const original_width = out.inner().width();
		out << std::setw(0);

		for (InputIt it = begin(); it != end();)
		{
			print_separating_line();
			print_single_row(it);
		}

		print_separating_line();

		out << std::setw(original_width);
	}

	template <typename InputIt>
	InputIt
	PrettyGrid<InputIt>::begin () const
	{
		return _begin;
	}

	template <typename InputIt>
	InputIt
	PrettyGrid<InputIt>::end () const
	{
		return _end;
	}

	template <typename InputIt>
	unsigned
	PrettyGrid<InputIt>::max_elements_per_row () const
	{
		return _max_elements_per_row;
	}

	template <typename InputIt>
	unsigned
	PrettyGrid<InputIt>::entry_width () const
	{
		return _entry_width;
	}

	template <typename InputIt>
	PrettyGrid<InputIt>
	as_pretty_grid (InputIt begin, InputIt end, unsigned max_elements_per_row, unsigned entry_width)
	{
		return PrettyGrid<InputIt>(begin, end, max_elements_per_row, entry_width);
	}

	//END: Template implementation

} // namespace UK

#endif /* LIBUKODUS_GRID_PRINTER_HPP */
