//
// Created by Tomas Silveira Salles on 26/05/17.
//

#ifndef LIBUKODUS_OSTREAM_WRAPPER_HPP
#define LIBUKODUS_OSTREAM_WRAPPER_HPP

/**
 * @file ostream_wrapper.hpp
 *
 * @author <a href="mailto:tomasssalles@gmail.com">Tomas Silveira Salles</a>
 */

#include <iostream>

namespace UK
{

	// Forward-declaration (resolved below)
	class OStream;

	/**
	 * @brief Fall-back version of @c output for the case it is not overloaded for @c T.
	 *
	 * This default version of @c output should be called whenever we do not overload it for the type
	 * @c T passed as second argument. It simply forwards @c t to the output operator of the internal
	 * @c std::ostream wrapped in the @c OStream.
	 */
	template <typename T>
	inline void
	output (OStream & out, T const & t);

	/**
	 * @class OStream
	 *
	 * This simple wrapper around an @c std::ostream allows us to redefine the output operator's
	 * behaviour by overloading our own global method @c output, without overloading or specializing
	 * anything in the @c std namespace, which can often cause headaches.
	 */
	class OStream final
	{
	public:

		/**
		 * @brief Construct an OStream wrapped around @c inner.
		 *
		 * @param inner_ A reference to the @c std::ostream to be wrapped.
		 */
		explicit OStream (std::ostream & inner_);

		/**
		 * @brief Output operator.
		 *
		 * Calls the global method <code>output(OStream &, T const &)</code>, which the user can
		 * overload for user-defined classes so as to print meaningful output. If not, the default
		 * implementation of @c output will be used, which just outputs the object to the inner
		 * @c std::ostream of this.
		 */
		template <typename T>
		OStream &
		operator<< (T const & t)
		{
			output(*this, t);
			return *this;
		}

		/** @brief Special overload for @c std::ostream manipulators */
		OStream &
		operator<< (std::ostream & (* func_ptr) (std::ostream &))
		{
			func_ptr(inner());
			return *this;
		}

		/** @return The wrapped @c std::ostream. */
		std::ostream &
		inner () const;

	private:
		std::ostream & _inner;
	};

	/** @brief Global static wrapper for @c std::cout */
	OStream &
	coutw ();

	/** @brief Global static wrapper for @c std::cerr */
	OStream &
	cerrw ();

	// BEGIN: Template implementation section.
	template <typename T>
	inline void
	output (OStream & out, T const & t)
	{
		out.inner() << t;
	}
	// END: Template implementation section.

} // namespace UK

#endif /* LIBUKODUS_OSTREAM_WRAPPER_HPP */
