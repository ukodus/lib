//
// Created by Tomas Silveira Salles on 28/05/17.
//

#ifndef LIBUKODUS_TIME_PRINTER_HPP
#define LIBUKODUS_TIME_PRINTER_HPP

/**
 * @file time_printer.hpp
 *
 * @author <a href="mailto:tomasssalles@gmail.com">Tomas Silveira Salles</a>
 */

#include <utils/ostream_wrapper.hpp>
#include <utils/retcode.hpp>

#include <chrono>

namespace UK
{

	/**
	 * @class PrettyTime
	 *
	 * @note Don't use this class directly. The factory method @ref as_pretty_time is much more convenient!
	 *
	 * This class template is a simple wrapper around an <code>std::chrono::duration<...></code> value whose sole
	 * purpose is to be written to an @c OStream and produce a nice looking, human-readable output.
	 *
	 * For example, the following fragment
	 * @code
	 * 	OStream & out = coutw();
	 * 	std::chrono::minutes const time(102);
	 * 	out << as_pretty_time(time) << std::endl;
	 * @endcode
	 *
	 * Will produce something like the following output
	 *
	 * @code
	 * 	1 hour and 42 minutes
	 * @endcode
	 *
	 * The exact details of the output are left undocumented, so small changes are possible in future versions.
	 *
	 * @warning
	 * 	- If the parameter passed to the constructor has value 0, the output will be some message like
	 * 	<code>"no time at all"</code>. If you are looking for something more formal you should previously check for
	 * 	this special case and treat it differently.
	 *		- There is some precision loss in the output text, as it is meant to be short. As of this version at
	 *		most two time units are used, and only for 'adjacent' units in the hierarchy, and only for the larger units.
	 * 	For example, 1 hour and 1 minute will be printed as "1 hour and 1 minute", but 1 hour and 1 second will be
	 * 	printed only as "1 hour", and 1 hour, 1 minute and 1 second will be printed as "1 hour and 1 minute". Then,
	 * 	for the shorter time units only 1 unit is used, so 1 microsecond and 1 nanosecond will be printed as
	 * 	"1 microsecond". Again, details are omitted, but the point is that some truncation is performed to keep the
	 * 	output short and nice.
	 *
	 * @tparam Rep The first template parameter of the wrapped std::chrono::duration type.
	 * @tparam Period The second template parameter of the wrapped std::chrono::duration type.
	 */
	template <typename Rep, typename Period>
	class PrettyTime final
	{
	public:

		/**
		 * @brief The wrapped @c std::chrono::duration type.
		 */
		using TimeType = std::chrono::duration<Rep, Period>;

		/**
		 * @note Don't use this directly. The factory method @ref as_pretty_time is much more convenient!
		 *
		 * @param base_time_ The duration value to be wrapped and eventually printed.
		 */
		explicit PrettyTime (TimeType base_time_);

		friend void
		output (OStream & out, PrettyTime<Rep, Period> const & pt)
		{
			pt.write_to(out);
		}

	private:
		void
		write_to (OStream & out) const;

		TimeType
		base_time () const;

		TimeType const _base_time;
	};

	/**
	 * Use this method to easily construct a @c PrettyTime object without having to write the template
	 * parameters explicitly. Example:
	 *
	 * @code
	 * 	OStream & out = coutw();
	 * 	std::chrono::minutes const time(102);
	 * 	out << as_pretty_time(time) << std::endl;
	 * @endcode
	 *
	 * @tparam Rep The first template parameter of the wrapped std::chrono::duration type.
	 * @tparam Period The second template parameter of the wrapped std::chrono::duration type.
	 * @param time The duration value to be wrapped and eventually printed.
	 * @return An instance of @c PrettyTime ready to be written to an @c OStream.
	 */
	template <typename Rep, typename Period>
	PrettyTime<Rep, Period>
	as_pretty_time (std::chrono::duration<Rep, Period> time);


	// BEGIN: Template implementation section.

	template <typename Rep, typename Period>
	PrettyTime<Rep, Period>::PrettyTime (TimeType base_time_)
		: _base_time(base_time_) {}

	template <typename Rep, typename Period>
	void
	PrettyTime<Rep, Period>::write_to (OStream & out) const
	{
		enum UnitRank { ur_first_unit, ur_second_unit };

		///////////////////////////////////////////////////////////////////////////////////////
		// Helpful lambdas: (A bit repetitive, but it would take some work to be able to
		// traverse the different time units and associate them with the different behaviours
		// and strings and so on.)

		auto print_hours = [this, &out] () -> RetCode
		{
			auto const value = std::chrono::duration_cast<std::chrono::hours>(base_time()).count();

			if (value != 0)
			{
				out << value;

				if (value == 1) { out << " hour"; }
				else { out << " hours"; }
			}

			return success_if(value != 0);
		};

		auto print_minutes = [this, &out] (UnitRank ur) -> RetCode
		{
			auto const value = std::chrono::duration_cast<std::chrono::minutes>(
				base_time() - std::chrono::duration_cast<std::chrono::hours>(base_time())).count();

			if (value != 0)
			{
				if (ur == ur_second_unit) { out << " and "; }

				out << value;

				if (value == 1) { out << " minute"; }
				else { out << " minutes"; }
			}

			return success_if(value != 0);
		};

		auto print_seconds = [this, &out] (UnitRank ur) -> RetCode
		{
			auto const value = std::chrono::duration_cast<std::chrono::seconds>(
				base_time() - std::chrono::duration_cast<std::chrono::minutes>(base_time())).count();

			if (value != 0)
			{
				if (ur == ur_second_unit) { out << " and "; }

				out << value;

				if (value == 1) { out << " second"; }
				else { out << " seconds"; }
			}

			return success_if(value != 0);
		};

		auto print_milliseconds = [this, &out] (UnitRank ur) -> RetCode
		{
			auto const value = std::chrono::duration_cast<std::chrono::milliseconds>(
				base_time() - std::chrono::duration_cast<std::chrono::seconds>(base_time())).count();

			if (value != 0)
			{
				if (ur == ur_second_unit) { out << " and "; }

				out << value;

				if (value == 1) { out << " millisecond"; }
				else { out << " milliseconds"; }
			}

			return success_if(value != 0);
		};

		auto print_microseconds = [this, &out] () -> RetCode
		{
			auto const value = std::chrono::duration_cast<std::chrono::microseconds>(
				base_time() - std::chrono::duration_cast<std::chrono::milliseconds>(base_time())).count();

			if (value != 0)
			{
				out << value;

				if (value == 1) { out << " microsecond"; }
				else { out << " microseconds"; }
			}

			return success_if(value != 0);
		};

		auto print_nanoseconds = [this, &out] () -> RetCode
		{
			auto const value = std::chrono::duration_cast<std::chrono::nanoseconds>(
				base_time() - std::chrono::duration_cast<std::chrono::microseconds>(base_time())).count();

			if (value != 0)
			{
				out << value;

				if (value == 1) { out << " nanosecond"; }
				else { out << " nanoseconds"; }
			}

			return success_if(value != 0);
		};

		////////////////////////////////////////////////
		// Tricky conditionals:

		if (print_hours() == RetCode::success)
		{
			print_minutes(ur_second_unit);
		}
		else if (print_minutes(ur_first_unit) == RetCode::success)
		{
			print_seconds(ur_second_unit);
		}
		else if (print_seconds(ur_first_unit) == RetCode::success)
		{
			print_milliseconds(ur_second_unit);
		}
		else if (print_milliseconds(ur_first_unit) == RetCode::success)
		{
			/* NOP */
		}
		else if (print_microseconds() == RetCode::success)
		{
			/* NOP */
		}
		else if (print_nanoseconds() == RetCode::success)
		{
			/* NOP */
		}
		else
		{
			out << "no time at all";
		}
	}

	template <typename Rep, typename Period>
	typename PrettyTime<Rep, Period>::TimeType
	PrettyTime<Rep, Period>::base_time () const
	{
		return _base_time;
	}

	template <typename Rep, typename Period>
	PrettyTime<Rep, Period>
	as_pretty_time (std::chrono::duration<Rep, Period> time)
	{
		return PrettyTime<Rep, Period>(time);
	}

	// END: Template implementation section.

} // namespace UK

#endif /* LIBUKODUS_TIME_PRINTER_HPP */
