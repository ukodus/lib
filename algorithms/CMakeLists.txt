project(algorithms)

set(SOURCES)

add_library(algorithms ${SOURCES})

include_directories(${utils_SOURCE_DIR} ${base_SOURCE_DIR})

target_link_libraries(algorithms base utils)
