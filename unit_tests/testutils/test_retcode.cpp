//
// Created by Tomas Silveira Salles on 28/05/17.
//

#include <gtest/gtest.h>

#include <utils/retcode.hpp>

#include <sstream>

TEST(retcode, success_if)
{
	EXPECT_EQ(UK::success_if(2 * 2 == 4), UK::RetCode::success);
	EXPECT_EQ(UK::success_if(2 * 2 == 5), UK::RetCode::failure);
}

TEST(retcode, output)
{
	std::ostringstream oss;
	UK::OStream out(oss);
	
	out << UK::RetCode::success << ' ' << UK::RetCode::failure;
	EXPECT_EQ(oss.str(), "success failure");
}
