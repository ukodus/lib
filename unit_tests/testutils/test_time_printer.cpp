//
// Created by Tomas Silveira Salles on 28/05/17.
//

#include <gtest/gtest.h>

#include <utils/time_printer.hpp>

#include <sstream>

TEST(time_printer, hours_and_minutes)
{
	std::ostringstream oss;
	UK::OStream out(oss);

	std::chrono::minutes const time(102);
	out << UK::as_pretty_time(time);

	EXPECT_EQ(oss.str(), "1 hour and 42 minutes");
}

TEST(time_printer, hours_no_minutes)
{
	std::ostringstream oss;
	UK::OStream out(oss);

	std::chrono::minutes const time(120);
	out << UK::as_pretty_time(time);

	EXPECT_EQ(oss.str(), "2 hours");
}

TEST(time_printer, minutes_and_seconds)
{
	std::ostringstream oss;
	UK::OStream out(oss);

	std::chrono::milliseconds const time(102'000);
	out << UK::as_pretty_time(time);

	EXPECT_EQ(oss.str(), "1 minute and 42 seconds");
}

TEST(time_printer, hours_no_seconds)
{
	std::ostringstream oss;
	UK::OStream out(oss);

	std::chrono::milliseconds const time(7'201'000);
	out << UK::as_pretty_time(time);

	EXPECT_EQ(oss.str(), "2 hours");
}

TEST(time_printer, minutes_and_seconds_no_milliseconds)
{
	std::ostringstream oss;
	UK::OStream out(oss);

	std::chrono::milliseconds const time(121'100);
	out << UK::as_pretty_time(time);

	EXPECT_EQ(oss.str(), "2 minutes and 1 second");
}

TEST(time_printer, seconds_and_milliseconds)
{
	std::ostringstream oss;
	UK::OStream out(oss);

	std::chrono::microseconds const time(5'121'100);
	out << UK::as_pretty_time(time);

	EXPECT_EQ(oss.str(), "5 seconds and 121 milliseconds");
}

TEST(time_printer, milliseconds_no_microseconds)
{
	std::ostringstream oss;
	UK::OStream out(oss);

	std::chrono::microseconds const time(121'100);
	out << UK::as_pretty_time(time);

	EXPECT_EQ(oss.str(), "121 milliseconds");
}

TEST(time_printer, microseconds_no_nanoseconds)
{
	std::ostringstream oss;
	UK::OStream out(oss);

	std::chrono::nanoseconds const time(121'100);
	out << UK::as_pretty_time(time);

	EXPECT_EQ(oss.str(), "121 microseconds");
}

TEST(time_printer, nanoseconds)
{
	std::ostringstream oss;
	UK::OStream out(oss);

	std::chrono::nanoseconds const time(1);
	out << UK::as_pretty_time(time);

	EXPECT_EQ(oss.str(), "1 nanosecond");
}

TEST(time_printer, no_time)
{
	std::ostringstream oss;
	UK::OStream out(oss);

	std::chrono::nanoseconds const time(0);
	out << UK::as_pretty_time(time);

	EXPECT_EQ(oss.str(), "no time at all");
}

TEST(time_printer, too_many_hours)
{
	std::ostringstream oss;
	UK::OStream out(oss);

	// On my machine durations above 2'562'048 hours cannot be represented as nanoseconds.
	// This test was introduced as PrettyTime was still not a class template, and the
	// constructor converted the given duration into nanoseconds. This test failed then,
	// and making PrettyTime into a class template was the solution.
	std::chrono::hours const time(std::numeric_limits<std::chrono::hours::rep>::max());
	out << UK::as_pretty_time(time);

	std::ostringstream expected_oss;
	expected_oss << time.count() << " hours";

	EXPECT_EQ(oss.str(), expected_oss.str());
}
