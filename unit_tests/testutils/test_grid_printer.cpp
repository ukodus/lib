//
// Created by Tomas Silveira Salles on 28/05/17.
//

#include <gtest/gtest.h>

#include <utils/grid_printer.hpp>

#include <sstream>
#include <array>

TEST(grid_printer, simple_example)
{
	std::ostringstream oss;
	UK::OStream out(oss);

	std::array<int, 8> const arr = {1, 2, 3, 4, 5, 6, 7, 8};
	out << UK::as_pretty_grid(arr.begin(), arr.end(), 3);

	// UK::cerrw() << ' ' << std::endl << "Output starts on next line:" << std::endl << oss.str() << std::endl;

	EXPECT_EQ(oss.str(),
	   "+---+---+---+\n"
		"| 1 | 2 | 3 |\n"
		"+---+---+---+\n"
		"| 4 | 5 | 6 |\n"
		"+---+---+---+\n"
		"| 7 | 8 |   |\n"
		"+---+---+---+\n");
}

TEST(grid_printer, single_column)
{
	std::ostringstream oss;
	UK::OStream out(oss);

	std::array<int, 8> const arr = {1, 2, 3, 4, 5, 6, 7, 8};
	out << UK::as_pretty_grid(arr.begin(), arr.end(), 1);

	// UK::cerrw() << ' ' << std::endl << "Output starts on next line:" << std::endl << oss.str() << std::endl;

	EXPECT_EQ(oss.str(),
	   "+---+\n"
		"| 1 |\n"
		"+---+\n"
		"| 2 |\n"
		"+---+\n"
		"| 3 |\n"
		"+---+\n"
		"| 4 |\n"
		"+---+\n"
		"| 5 |\n"
		"+---+\n"
		"| 6 |\n"
		"+---+\n"
		"| 7 |\n"
		"+---+\n"
		"| 8 |\n"
		"+---+\n");
}

TEST(grid_printer, single_row)
{
	std::ostringstream oss;
	UK::OStream out(oss);

	std::array<int, 8> const arr = {1, 2, 3, 4, 5, 6, 7, 8};
	out << UK::as_pretty_grid(arr.begin(), arr.end(), 10);

	// UK::cerrw() << ' ' << std::endl << "Output starts on next line:" << std::endl << oss.str() << std::endl;

	EXPECT_EQ(oss.str(),
	   "+---+---+---+---+---+---+---+---+---+---+\n"
		"| 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |   |   |\n"
		"+---+---+---+---+---+---+---+---+---+---+\n");
}

TEST(grid_printer, empty_array)
{
	std::ostringstream oss;
	UK::OStream out(oss);

	std::array<int, 0> const arr = {};
	out << UK::as_pretty_grid(arr.begin(), arr.end(), 3);

	// UK::cerrw() << ' ' << std::endl << "Output starts on next line:" << std::endl << oss.str() << std::endl;

	EXPECT_EQ(oss.str(),
	   "+---+---+---+\n");
}

TEST(grid_printer, width)
{
	std::ostringstream oss;
	UK::OStream out(oss);

	std::array<int, 8> const arr = {1, 22, 333, 4444, 55555, 666666, 7777777, 88888888};
	out << UK::as_pretty_grid(arr.begin(), arr.end(), 3, 8);

	// UK::cerrw() << ' ' << std::endl << "Output starts on next line:" << std::endl << oss.str() << std::endl;

	EXPECT_EQ(oss.str(),
	   "+----------+----------+----------+\n"
		"|        1 |       22 |      333 |\n"
		"+----------+----------+----------+\n"
		"|     4444 |    55555 |   666666 |\n"
		"+----------+----------+----------+\n"
		"|  7777777 | 88888888 |          |\n"
		"+----------+----------+----------+\n");
}
