//
// Created by Tomas Silveira Salles on 27/05/17.
//

#include <gtest/gtest.h>

#include <utils/ostream_wrapper.hpp>
#include <sstream>

TEST(ostream_wrapper, normal_output)
{
	std::ostringstream oss;
	UK::OStream out(oss);

	out << "Hello, World!";
	EXPECT_EQ(oss.str(), "Hello, World!");
}

TEST(ostream_wrapper, manipulator)
{
	std::ostringstream oss;
	UK::OStream out(oss);

	out << true << ' ' << std::boolalpha << true;
	EXPECT_EQ(oss.str(), "1 true");
}

namespace
{
	struct PrettyNumber
	{
		unsigned n;

		PrettyNumber (unsigned n_)
			: n(n_) {}

		friend void output(UK::OStream & out, PrettyNumber const & pn)
		{
			switch (pn.n)
			{
				case 0: out << "zero"; break;
				case 1: out << "one"; break;
				case 2: out << "two"; break;
				default: out << "large number"; break;
			}
		}
	};
}

TEST(ostream_wrapper, custom_output)
{
	std::ostringstream oss;
	UK::OStream out(oss);

	out << PrettyNumber(0) << ", " << PrettyNumber(1) << ", " << PrettyNumber(2) << ", " << PrettyNumber(3);
	EXPECT_EQ(oss.str(), "zero, one, two, large number");
}

TEST(ostream_wrapper, coutw_address)
{
	EXPECT_EQ(&(UK::coutw().inner()), &(std::cout));
}

TEST(ostream_wrapper, cerrw_address)
{
	EXPECT_EQ(&(UK::cerrw().inner()), &(std::cerr));
}
