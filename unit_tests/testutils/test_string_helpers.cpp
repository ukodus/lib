//
// Created by Tomas Silveira Salles on 27/05/17.
//

#include <gtest/gtest.h>

#include <utils/string_helpers.hpp>

TEST(string_helpers, normal_use)
{
	std::string str = "Hello, World!";
	EXPECT_EQ(UK::StringUtils::remove_characters(str, {'l', 'o'}), "He, Wrd!");
}

TEST(string_helpers, empty_characters_list)
{
	std::string str = "Hello, World!";
	EXPECT_EQ(UK::StringUtils::remove_characters(str, {}), "Hello, World!");
}

TEST(string_helpers, non_present_character)
{
	std::string str = "Hello, World!";
	EXPECT_EQ(UK::StringUtils::remove_characters(str, {'l', 'o', '?', 'f'}), "He, Wrd!");
}

TEST(string_helpers, empty_string)
{
	std::string str = "";
	EXPECT_EQ(UK::StringUtils::remove_characters(str, {'a', 'b'}), "");
}
